package com.example.gpssocketserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class MyTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyTask.class);

    private ExecutorService executorService;
    private Socket socket;

    public MyTask(ExecutorService executorService, Socket socket) {
        this.executorService = executorService;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            final Scanner socketIn = new Scanner(socket.getInputStream());
            final PrintStream socketOut = new PrintStream(socket.getOutputStream());

            while (socketIn.hasNextLine()) {
                final String message = socketIn.nextLine();
                LOGGER.info("Message received {}", message);

                final String[] splittedMessage = message.split(",");
                String response = "";
                switch (splittedMessage.length) {
                    case 1: // 359710049095095 -> heartbeat requires "ON" response
                        response = "ON";
                        LOGGER.info("SENT ON TO CLIENT");
                        break;
                    case 3: // ##,imei:359710049095095,A -> this requires a "LOAD" response
                        if ("##".equals(splittedMessage[0])) {
                            response = "LOAD";
                            LOGGER.info("SENT LOAD TO CLIENT");
                        }
                        break;
                    case 19: // imei:359710049095095,tracker,151006012336,,F,172337.000,A,5105.9792,N,11404.9599,W,0.01,322.56,,0,0,,,  -> this is our gps data
                        // save to database
                        break;
                }

                if (response.length() > 0) {
                    socketOut.println(response);
                }
            }

            LOGGER.info("CLOSING CONNECTION");
            socketIn.close();
            socketOut.close();
        } catch (IOException e) {
            LOGGER.error("", e);
            throw new RuntimeException(e);
        }
    }
}
