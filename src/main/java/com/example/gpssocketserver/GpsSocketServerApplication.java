package com.example.gpssocketserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@SpringBootApplication
public class GpsSocketServerApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(GpsSocketServerApplication.class);

    private static final int SERVER_PORT = 60000;
    private static final int NUMBER_OF_THREADS = 10;

    private ServerSocket serverSocket;
    private AtomicBoolean isRunning;
    private ExecutorService executorService;

	public static void main(String[] args) {
		SpringApplication.run(GpsSocketServerApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
	    startServer();
	    runServer();
    }

    private void startServer() throws IOException {
	    serverSocket = new ServerSocket(SERVER_PORT);
	    isRunning = new AtomicBoolean(true);
	    executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    }

    private void runServer() {
	    while (isRunning.get()) {
            try {
                Socket socket = serverSocket.accept();
                LOGGER.info("New client connected");
                executorService.execute(new MyTask(executorService, socket));
            } catch (IOException e) {
                LOGGER.error("", e);
                throw new RuntimeException(e);
            }
        }
    }

	private void stopServer() throws IOException {
	    LOGGER.info("Stopping server");
        serverSocket.close();
        LOGGER.info("Server stopped");
    }
}
