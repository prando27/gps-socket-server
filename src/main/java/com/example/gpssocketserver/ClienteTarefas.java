package com.example.gpssocketserver;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by prando on 09/07/17.
 */
public class ClienteTarefas {

    public static void main(String[] args) throws Exception {

        Socket socket = new Socket("34.207.53.89", 60000);
        System.out.println("conexao estabelecida");

        Thread threadEnviaComando = new Thread(() -> {

            try {
                System.out.println("Pode enviar comandos!");
                PrintStream saida = new PrintStream(socket.getOutputStream());

                // aguardando enter
                Scanner teclado = new Scanner(System.in);
                while (teclado.hasNextLine()) {
                    String linha = teclado.nextLine();

                    if ("".equals(linha)) {
                        break;
                    }

                    saida.println(linha);
                }

                //fechando recursos
                saida.close();
                teclado.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        Thread threadRecebeResposta = new Thread(() -> {

            try {
                System.out.println("Recebendo dados do servidor");
                Scanner respostaServidor = new Scanner(socket.getInputStream());

                while (respostaServidor.hasNextLine()) {
                    String linha = respostaServidor.nextLine();
                    System.out.println(linha);
                }

                respostaServidor.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        threadRecebeResposta.start();
        threadEnviaComando.start();

        // a thread main vai esperar a threadRecebeResposta terminar
        threadEnviaComando.join();

        System.out.println("Fechando o socket do cliente");
        socket.close();
    }
}
